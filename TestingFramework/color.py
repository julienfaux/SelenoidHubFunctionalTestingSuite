from colorama import Fore


class Color:

    @staticmethod
    def blue(s):
        return f"{Fore.LIGHTBLUE_EX}{s}{Fore.RESET}"

    @staticmethod
    def green(s):
        return f"{Fore.GREEN}{s}{Fore.RESET}"

    @staticmethod
    def orange(s):
        return f"{Fore.YELLOW}{s}{Fore.RESET}"

    @staticmethod
    def red(s):
        return f"{Fore.RED}{s}{Fore.RESET}"

    @staticmethod
    def white(s):
        return f"{Fore.WHITE}{s}{Fore.RESET}"

    @staticmethod
    def yellow(s):
        return f"{Fore.LIGHTYELLOW_EX}{s}{Fore.RESET}"
