from enum import Enum
from . import __version__
from .wd import Wd, capabilities
from .log import logger
from sys import exit
from .color import Color

log = logger('HANDLER')


class TestStates(Enum):
    INIT = -2
    RUNNING = -1
    SUCCESS = 0
    FAILURE = 1
    ERROR = 2
    MAINTENANCE = 3


class Handler:
    def __init__(self, scenario_name, wd_required=True):
        self.test_state = ""
        self.scenario_name = scenario_name
        self.wd = Wd(mode='remote', capa=capabilities) if wd_required else None
        self._run()

    def _ignition(self):
        log.info(f"TestingFramework {__version__} running {self.scenario_name}")
        if self.wd:
            self.wd.wd_ignition()

    def _finalize(self):
        self.test_init()
        if self.test_state in [TestStates.INIT.name, TestStates.RUNNING.name]:
            self.test_ok()
        if self.wd:
            self.wd.kill_wd()
        exit(TestStates[self.test_state].value)

    def _run(self):
        try:
            self._ignition()
            self.main_test()
            self._finalize()
        except Exception as e:
            log.error(f"during run: {e}")
            self.test_in_maintenance()

    def main_test(self):
        jobs = []

        for method in dir(self):
            if str(method).startswith("job_"):
                jobs.append(method)

        jobs.sort()

        for job in jobs:
            exec(f"self.{job}()")

    def depiler(self, task_list, all=False):
        for task in task_list:
            if self.test_state in [TestStates.SUCCESS.name, TestStates.INIT.name, TestStates.RUNNING.name] or all:
                try:
                    log.debug(f"depiling {str(task)}...")
                    task()
                except Exception as e:
                    log.error(f"exception catched during {str(task)} : {e}")
                    self.test_error()

    def test_init(self, label=""):
        log.debug(Color.blue(f"{self.scenario_name}{' : ' + label if label else ''}: INIT"))
        self.test_state = TestStates.INIT.name

    def test_ok(self, label=""):
        log.info(Color.green(f"{self.scenario_name}{' : ' + label if label else ''}: OK"))
        self.test_state = TestStates.SUCCESS.name

    def test_error(self, label=""):
        log.error(Color.orange(f"{self.scenario_name}{' : ' + label if label else ''} : ERROR"))
        self.test_state = TestStates.ERROR.name

    def test_ko(self, label=""):
        log.error(Color.red(f"{self.scenario_name}{' : ' + label if label else ''} : KO"))
        self.test_state = TestStates.FAILURE.name

    def test_in_maintenance(self, label=""):
        log.error(Color.orange(f"{self.scenario_name}{' : ' + label if label else ''} : MAINTENANCE EREQUIRED"))
        self.test_state = TestStates.MAINTENANCE.name
