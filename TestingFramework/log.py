import logging.config

log_config = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': '%(process)s | %(asctime)s  | %(levelname)s | %(name)s | %(message)s'}
    }, 'handlers': {
        'console': {
            'class': 'logging.StreamHandler', 'formatter': 'standard', 'stream': 'ext://sys.stdout'}},
    'loggers': {
        'uvicorn': {
            'error': {
                'propagate': True}}},
    'root': {
        'level': 'INFO', 'handlers': ['console'], 'propagate': False}}

logging.config.dictConfig(log_config)

logger = logging.getLogger
