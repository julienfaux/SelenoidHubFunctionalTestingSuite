import json
from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from ..TestingFramework.log import logger
import requests

HUB = "http://127.0.0.1:4444/wd/hub"

EVENTS = {
    "presence": expected_conditions.presence_of_element_located,
    "visibility": expected_conditions.visibility_of,
    "click": expected_conditions.element_to_be_clickable
}

log = logger('WD')


def latest_browser(browser, hub_url):
    req = requests.get(hub_url.replace('/wd/hub', "/status"))
    result = json.loads(req.text)
    if result['browsers'][browser]:
        return max(result['browsers'][browser].keys())


def spawn_wd(capa, hub="", mode='remote'):
    if mode == 'remote':
        wd = webdriver.Remote(command_executor=hub, desired_capabilities=capa)

    elif mode == 'local':
        from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
        fb = FirefoxBinary("/home/julien-es/code/firefox/firefox")
        wd = webdriver.Firefox(firefox_binary=fb)
    else:
        raise RuntimeError(f'unknown mode {mode}')
    wd.maximize_window()
    return wd


firefox_binary_path = "/home/j/code/firefox/firefox"
current_browser = 'chrome'
capabilities = {
    "browserName": current_browser,
    "browserVersion": latest_browser(browser=current_browser, hub_url=HUB),
    'idleTimeout': 600,
    "selenoid:options": {
        "enableVNC": True,
        "enableVideo": False
    },
    "allowed-origins": "*"
}


class Wd:
    def __init__(self, mode='remote', browser='chrome', capa=None):
        self._wd = None
        self.browser_name = browser if browser else None
        self.capabilities = capa
        self.mode = mode
        self.by = By
        self.keys = Keys
        self.ac = ActionChains

    def _spawn_wd(self, capa, hub="", mode='remote'):
        try:
            capa = self.capabilities if capa is None else capa
            if mode == 'remote':
                wd = webdriver.Remote(command_executor=hub, desired_capabilities=capa)

            elif mode == 'local':
                if capa["browserName"] == 'firefox':
                    from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
                    self.browser_name = "firefox"
                    fb = FirefoxBinary(firefox_binary_path)
                    wd = webdriver.Firefox(firefox_binary=fb)
                else:
                    raise RuntimeError(f'unable to launch {mode} {capa["browserName"]}')
            else:
                raise RuntimeError(f'unknown mode {mode}')

            if wd:
                wd.maximize_window()
                log.info(f"{mode} {capa['browserName']} {capa['browserVersion']} spawned.")
                return wd
        except Exception as e:
            log.error(f'could not spawn webdriver : {e}')

    def _action(self):
        return ActionChains(self._wd)

    def url(self):
        return self._wd.current_url

    def click_button(self, label, timeout=3, blank=False):
        self.wd_wait(by=self.by.TAG_NAME, value="button", timeout=timeout, method="click")
        for button in self.wd_wait(by=self.by.TAG_NAME, value="button", all=True, timeout=timeout):
            if button.text.lower() == label.lower():
                self.ac = self._action()
                self.ac.move_to_element(button).perform()
                sleep(2)

                if not blank:
                    button.click()

    def wd_wait(self, by: By, value, timeout=40, all=False, method="presence"):
        if method not in EVENTS.keys():
            raise RuntimeError(f"wd_wait : unknowwn method {method}")
        try:
            WebDriverWait(self._wd, timeout).until(EVENTS[method]((by, value)))
            return self._wd.find_elements(by=by, value=value) if all else self._wd.find_element(by=by, value=value)

        except Exception as e:
            log.warning(f"wd_wait : by: {by}, value: {value}, timeout: {timeout}, all: {all}")
            log.warning(f"wd_wait : {e}")

    def kill_wd(self):
        try:
            self._wd.quit()
        except Exception as e:
            log.error(f"{e} during killing.")

    def get(self, url):
        log.debug(f"get {url}")
        self._wd.get(url)

    def scroll_down(self):
        log.debug(f"scrolling down...")
        self._wd.execute_script("window.scrollTo(0, document.body.scrollHeight);")

    def scroll_into_view(self, wd_selector):
        self._wd.execute_script("arguments[0].scrollIntoView();", wd_selector)

    def wd_ignition(self):
        log.debug(f"ignition...")
        self.browser_name = self.capabilities['browserName'] if self.browser_name is None else self.browser_name
        self._wd = self._spawn_wd(hub=HUB, capa=self.capabilities, mode=self.mode)

    def html_dump(self):
        return self._wd.execute_script("return document.getElementsByTagName('html')[0].innerHTML")
