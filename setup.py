from setuptools import find_packages, setup
from TestingFramework import __version__

setup(
    name="SelenoidHubFunctionalTestingSuite",
    description="Simple functional testing framework using selenoid selenium hub written in python",
    author="Julien Faux",
    author_email="julienfaux@gmail.com",
    url="https://gitlab.com/julienfaux/SelenoidHubFunctionalTestingSuite",
    version=__version__,
    package_dir={"TestingFramework": "SelenoidHubFunctionalTestingSuite/TestingFramework"},
    packages=find_packages(exclude=("tests"))
)
